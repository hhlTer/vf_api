package apiservice.searchservice;


import com.vodafone.endpoints.AbstractEndpoint;
import com.vodafone.service.search.SearchDataDto;
import com.vodafone.service.search.SearchResponseDto;
import com.vodafone.tescases.common.service.search.BaseSearchApiTestCase;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.HttpStatus;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import java.util.Objects;
import java.util.function.Predicate;


@Slf4j
public class SearchAPITest extends BaseSearchApiTestCase {

    @Test
    public void positiveQueryTest() {
        final var validQuery = "Service";
        final var searchResponse = searchActions.searchAndGetResponse(validQuery);
        final var searchResult = AbstractEndpoint
                .extractAsDto(searchResponse, SearchDataDto.class).getData();

        Assertions
                .assertThat(searchResponse.extract().statusCode())
                .as("Verify if status code is 200")
                .isEqualTo(HttpStatus.SC_OK);

        Assertions
                .assertThat(searchResult)
                .as("Verify the searching result doesn't empty for query " + validQuery)
                .isNotEmpty();
    }

    @Test
    public void negativeQueryTest() {
        final var badQuery = "<a href/>";
        final var searchResult = searchActions.searchAndGetResponse(badQuery);
        Assertions.assertThat(searchResult.extract().statusCode())
                .as("Verify if status code is 404")
                .isEqualTo(HttpStatus.SC_NOT_FOUND);
    }

    //Supposed the test must be executed on the 'test' stage with famous and static result
    @Test
    public void paginationTest() {
        final var FIRST_PAGE_PAGINATION = 1;
        final var SECOND_PAGE_PAGINATION = 2;
        final var query = "Послуги";
        final var firstPageResult = searchActions.search(query, FIRST_PAGE_PAGINATION);
        final var secondPageResult = searchActions.search(query, SECOND_PAGE_PAGINATION);

        Assertions.assertThat(firstPageResult.size())
                .as("Verify if amount of pagination of the first and second page are equal.")
                .isEqualTo(secondPageResult.size());
    }

    @Test
    public void verifyContentTest() {
        final var query = "Послуги";
        final var validationString = "послуг";
        final var searchResult = searchActions.search(query);

        final Predicate<SearchResponseDto> isResponseCorrespondsToQuery = dto ->
                dto.getTitle().toLowerCase().contains(validationString) ||
                dto.getShortDescription().toLowerCase().contains(validationString);
        final Predicate<SearchResponseDto> isResponseContainsMainBreadcrumb = dto ->
                dto.getBreadcrumbs().contains("<a href=\"https://www.vodafone.ua/\">Головна</a>");
        final Predicate<SearchResponseDto> isIdNotNull = dto -> Objects.nonNull(dto.getId());

        Assertions.assertThat(searchResult)
                .as("Verify if response isn't empty")
                .isNotEmpty()
                .as("Verify if each response contains query parameter")
                .allMatch(isResponseCorrespondsToQuery)
                .as("Verify if breadcrumbs contains Main Page breadcrumb")
                .allMatch(isResponseContainsMainBreadcrumb)
                .as("Verify id's")
                .allMatch(isIdNotNull);
    }

    @Test
    public void emptyResultTest(){
        final var query = "notExistRandomWrongQuery123456789";
        final var searchResponse = searchActions.searchAndGetResponse(query);
        final var searchResult = AbstractEndpoint
                .extractAsDto(searchResponse, SearchDataDto.class).getData();

        Assertions.assertThat(searchResponse.extract().statusCode())
                .as("Verify that status code is 200 for empty result")
                .isEqualTo(HttpStatus.SC_OK);

        Assertions.assertThat(searchResult)
                .as("Verify that result is empty")
                .isEmpty();
    }
}
