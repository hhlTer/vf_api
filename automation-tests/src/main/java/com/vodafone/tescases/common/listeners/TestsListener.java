package com.vodafone.tescases.common.listeners;

import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

@Slf4j
public class TestsListener implements ITestListener {
    @Override
    public void onStart(ITestContext context) {
        log.info("The {} is started", context.getName());
    }

    @Override
    public void onFinish(ITestContext context) {
        log.info("The {} is finished", context.getName());
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        log.error("The {} test is failed", getTestMethodName(iTestResult));
        iTestResult.getThrowable().printStackTrace();
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        log.info("The {} test started", getTestMethodName(iTestResult));
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        log.info("The {} test succeed", getTestMethodName(iTestResult));
    }
    private String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }
}
