package com.vodafone.tescases.common.service.search;

import com.vodafone.service.search.testcases.actions.SearchEndpointActions;
import com.vodafone.tescases.common.listeners.TestsListener;
import com.vodafone.tescases.common.service.BaseApiTestCase;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;

@Listeners({TestsListener.class})
public class BaseSearchApiTestCase extends BaseApiTestCase {
    protected SearchEndpointActions searchActions;

    @BeforeClass(alwaysRun = true)
    public void setupActions() {
        this.searchActions = new SearchEndpointActions();
    }
}
