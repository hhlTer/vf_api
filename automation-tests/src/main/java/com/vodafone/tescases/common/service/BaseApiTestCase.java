package com.vodafone.tescases.common.service;


import org.testng.annotations.BeforeSuite;

//base testcase for API testing with @before.. and @After... setup fixtures for global config
public class BaseApiTestCase {
    @BeforeSuite(alwaysRun = true)
    public void suiteSetup(){
        //some global config
    }
}
