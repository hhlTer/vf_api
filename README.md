The idea of the project is that the different modules might be reused in the other parts of the product as a separate dependencies.

The suit defined in the automation-tests/src/test/resources/smoke.xml
To execute tests: `mvn clean test`

Logs: automation-tests/log4j-test-automation.log

* Some tests developed from understanding that tests will be executed on the 'tests' stage with famous data and expected results