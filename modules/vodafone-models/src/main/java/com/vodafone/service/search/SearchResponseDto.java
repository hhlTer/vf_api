package com.vodafone.service.search;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "title",
    "short_description",
    "url",
    "breadcrumbs"
})
@Data
@NoArgsConstructor
public class SearchResponseDto {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("short_description")
    private String shortDescription;
    @JsonProperty("url")
    private String url;
    @JsonProperty("breadcrumbs")
    private String breadcrumbs;

}