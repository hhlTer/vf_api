package com.vodafone.endpoints;

import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import java.util.HashMap;
import java.util.Map;

public class SearchEndpoint extends AbstractEndpoint {

    private static final String SEARCH_PATH = "/search/{query}";
    private static final String PAGINATION_PARAM = "page";

    public SearchEndpoint(RequestSpecification requestSpecification) {
        this.requestSpecification = requestSpecification;
    }

    public ValidatableResponse executeSearch(String query) {
        final var page = 1;
        return executeSearch(query, page);
    }

    public ValidatableResponse executeSearch(String query, int page) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put(PAGINATION_PARAM, page);
        return super.getWithParams(requestSpecification, SEARCH_PATH, paramsMap, query);
    }

}
