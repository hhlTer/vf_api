package com.vodafone.endpoints;

import static io.restassured.RestAssured.given;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.common.mapper.TypeRef;
import io.restassured.mapper.ObjectMapperType;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

import java.util.*;

abstract public class AbstractEndpoint {

    protected RequestSpecification requestSpecification;

    public ValidatableResponse post(RequestSpecification requestSpecification, String path, Object bodyPayload,
                                    Object... pathParams) {
        RequestSpecBuilder specBuilder = new RequestSpecBuilder();
        specBuilder.addRequestSpecification(requestSpecification);

        if (bodyPayload != null) {
            specBuilder.setBody(bodyPayload);
        }
        return given()
                .spec(specBuilder.build())
                .when()
                .post(path, pathParams)
                .then();
    }

    /**
     * Execute PUT request.
     *
     * @param requestSpecification RequestSpecification - holds rest assured information about request
     * @param path                 - path to endpoint, such as foo/bar
     * @param bodyPayload          the payload body to be sent in put
     * @param params               path params, for example if path contains /foo/{pathparam1}/bar/{pathparam2}, then
     *                             parameters will replaced in path.
     * @return ValidatableResponse
     */
    public ValidatableResponse put(RequestSpecification requestSpecification, String path, Object bodyPayload,
                                   Object... params) {
        RequestSpecBuilder specBuilder = new RequestSpecBuilder();
        specBuilder.addRequestSpecification(requestSpecification);

        if (bodyPayload != null) {
            specBuilder.setBody(bodyPayload);
        }

        return given()
                .spec(specBuilder.build())
                .when()
                .put(path, params)
                .then();
    }

    /**
     * Execute GET request.
     *
     * @param path   - path to endpoint, such as foo/bar
     * @param params path params, for example if path contains /foo/{pathparam1}/bar/{pathparam2}, then parameters will
     *               replaced in path.
     * @return ValidatableResponse
     */
    public ValidatableResponse get(String path, String... params) {
        return get(this.requestSpecification, path, params);
    }

    public ValidatableResponse get(RequestSpecification requestSpecification, String path, Object... pathParams) {
        return given()
                .spec(requestSpecification)
                .when()
                .get(path, pathParams)
                .then();
    }

    public ValidatableResponse getWithParams(RequestSpecification requestSpecification, String path, Map<String, Object> paramsMap, Object... pathParams) {
        return given()
                .spec(requestSpecification)
                .when()
                .params(paramsMap)
                .get(path, pathParams)
                .then();
    }

    /**todo: delete
     * Execute DELETE request.
     *
     * @param requestSpecification RequestSpecification - holds rest assured information about request
     * @param path                 - path to endpoint, such as foo/bar
     * @param params               path params, for example if path contains /foo/{pathparam1}/bar/{pathparam2}, then
     *                             parameters will replaced in path.
     * @return ValidatableResponse
     */
    public ValidatableResponse delete(RequestSpecification requestSpecification, String path, Object... params) {
        return given()
                .spec(requestSpecification)
                .when()
                .delete(path, params)
                .then();
    }

    /**
     * A helper method to use rest assured to extract validatable response as desired object type.
     */
    public static <T> T extractAsDto(ValidatableResponse validatableResponse, Class<T> dtoClass) {
        return validatableResponse.extract().as(dtoClass, ObjectMapperType.JACKSON_2);
    }

    /**
     * A helper method to use rest assured to extract validatable response as desired object type.
     */
    public static <T> T extractAsDto(ValidatableResponse validatableResponse, TypeRef<T> type) {
        return validatableResponse.extract().as(type);
    }

    /**
     * A helper method to use rest assured to extract validatable response as desired object list.
     */
    public static <T> List<T> extractAsDtoList(ValidatableResponse validatableResponse, Class<T> dtoClass) {
        return validatableResponse.extract().body().jsonPath().getList("", dtoClass);
    }
}
