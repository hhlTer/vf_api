package com.vodafone;

import com.vodafone.endpoints.SearchEndpoint;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public class VodafoneAPI {
    private static final ThreadLocal<VodafoneAPI> WEB_API = new ThreadLocal<>();
    private static final String BASE_PATH = "https://www.vodafone.ua/api";
    private SearchEndpoint searchEndpoint;

    private VodafoneAPI() {
        initEndpoints();
    }

    public static VodafoneAPI petStoreApi() {
        if (WEB_API.get() == null) {
            WEB_API.set(new VodafoneAPI());
        }
        return WEB_API.get();
    }

    public SearchEndpoint getSearchEndpoint() {
        return searchEndpoint;
    }
    private void initEndpoints(){
        RequestSpecification reqSpec = getDefaultSpecification();
        initPetEndpoint(reqSpec);
    }

    private void initPetEndpoint(RequestSpecification requestSpecification) {
        this.searchEndpoint = new SearchEndpoint(requestSpecification);
    }

    //better move this to some abstract endpoint config and make it more generic
    private RequestSpecification getDefaultSpecification() {
        return new RequestSpecBuilder()
            .setBaseUri(BASE_PATH)
            .build();
    }

    public static void remove() {
        WEB_API.remove();
    }

}
