package com.vodafone.service.search.testcases.actions;

import com.vodafone.service.search.SearchDataDto;
import com.vodafone.service.search.SearchResponseDto;
import io.restassured.response.ValidatableResponse;

import java.util.List;

import static com.vodafone.VodafoneAPI.petStoreApi;
import static com.vodafone.endpoints.AbstractEndpoint.extractAsDto;

public class SearchEndpointActions {
    public List<SearchResponseDto> search(String query) {
        var response = petStoreApi().getSearchEndpoint().executeSearch(query);
        return extractAsDto(response, SearchDataDto.class).getData();
    }
    public List<SearchResponseDto> search(String query, int page) {
        var response = petStoreApi().getSearchEndpoint().executeSearch(query, page);
        return extractAsDto(response, SearchDataDto.class).getData();
    }

    public ValidatableResponse searchAndGetResponse(String query){
        final int FIRST_PAGE = 1;
        return searchAndGetResponse(query, FIRST_PAGE);
    }

    public ValidatableResponse searchAndGetResponse(String query, int page){
        return petStoreApi().getSearchEndpoint().executeSearch(query, page);
    }
}
